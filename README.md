# Flaconi System

### Features
- Semantic HTML, List is a list, no div soup
- Modern ReactJS with hooks, including useReducer. No redux.
- Filtering, sorting and pagination
- State aware reducer, no inconsistent states where loading is triggered  twice etc (inspired by [This tweet](https://twitter.com/DavidKPiano/status/1171062893984526336))
- Comes with tests
- CI/CD integrated, auto-deploys (server is manual deploy)
- Comes with docker

## Check it live 
Live at [https://icemelt7.gitlab.io/flaconi](https://icemelt7.gitlab.io/flaconi)

## Running with docker 

Run the following commands

`docker build -t <your_username>/flaconi-test .`

`docker run -d -p 3333:3000 <your_username>/flaconi-test:latest`

Access the site on [http://localhost:3333](http://localhost:3333)

## Running without Docker

Run the following commands

`npm install`

`node server/index.js`

`npm run start`

Access the site on [http://localhost:3000](http://localhost:3000)
Access the server on [http://localhost:4000](http://localhost:4000)