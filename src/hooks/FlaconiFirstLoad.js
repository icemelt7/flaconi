import { useEffect } from 'react';
import { getAllBrandsAndTypes } from '../helpers/flaconiNetworkHelper';

const useFirstLoad = (dispatch) => {
  useEffect(() => {
    // Trigger a load, so I load the first page
    dispatch({
      type: "TRIGGER_LOAD"
    });

    // Pre-load all the brands and types 
    getAllBrandsAndTypes().then(brandsAndTypes => {
      dispatch({
        type: "LOAD_ALL_TYPES_AND_BRANDS",
        payload: brandsAndTypes.data
      });
    });
  }, [dispatch]);
}

export default useFirstLoad;