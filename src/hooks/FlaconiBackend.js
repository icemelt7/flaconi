import { useReducer, useEffect } from "react";
import { url, initialState } from "../constants";
import FlaconiUiReducer from "../reducers/FlaconiUiReducer";
import {
  createQuery,
  createHelperMethods,
  fetchAndLoadResults
} from "../helpers/flaconiNetworkHelper";
import useFirstLoad from "./FlaconiFirstLoad";

const useFlaconiBackend = () => {
  const [values, dispatch] = useReducer(FlaconiUiReducer, initialState);

  // First load
  useFirstLoad(dispatch);

  // Main Logic, that triggers load if any filters change
  useEffect(() => {
    // Only run fetch if the status is trigger load
    if (values.uistatus !== "triggerload") {
      return;
    }

    // Show loading
    dispatch({
      type: "LOADING"
    });

    // Create the query;
    const opts = createQuery(values.filters);

    // Fetch and load the results
    fetchAndLoadResults(url, opts, dispatch);
  }, [values.filters, values.uistatus]);

  // Actions for UI
  const {
    filterByBrand,
    filterByType,
    sortBy,
    goToPage,
    resetFilters
  } = createHelperMethods(dispatch);

  return {
    values,
    filterByBrand,
    filterByType,
    sortBy,
    goToPage,
    resetFilters
  };
};

export default useFlaconiBackend;
