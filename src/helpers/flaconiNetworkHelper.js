import { url, ITEMS_PER_PAGE } from "../constants";

/* Take all the filters from the object, like this { key: value }
   and convert them to bracket format like this (key: value, key value) */
export const createQuery = filters => {
  let arg = Object.entries(filters)
    .map(c => {
      if (c[0] === "page") {
        return `offset: ${(c[1] - 1) * ITEMS_PER_PAGE}`;
      } else {
        return `${c[0]}: "${c[1]}"`;
      }
    })
    .join(",");

  const query = `
    query {
      products(${arg}) {
        name
        slug
        brand
        type
        image
        price
        size
        rating
      }
    }
  `;

  const opts = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ query })
  };

  return opts;
};

export const getAllBrandsAndTypes = () => {
  const query = `
    query {
      brands,
      types
    }
  `;

  const opts = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ query })
  };

  return new Promise((resolve, reject) => {
    fetch(url, opts)
      .then(res => res.json())
      .then(data => resolve(data))
      .catch(e => reject(e));
  });
};

export const createHelperMethods = dispatch => {
  const filterByBrand = brand => {
    dispatch({
      type: "FILTER_BY_BRAND",
      payload: { brand }
    });
  };

  const filterByType = type => {
    dispatch({
      type: "FILTER_BY_TYPE",
      payload: { type }
    });
  };

  const sortBy = sort => {
    dispatch({
      type: "SORT_BY",
      payload: { sort: sort.target.value }
    });
  };

  const goToPage = page => {
    dispatch({
      type: "NAVIGATE_TO_PAGE",
      payload: { page }
    });
  }

  const resetFilters = () => {
    dispatch({
      type: "RESET_FILTERS"
    });
  }

  return {
    filterByBrand,
    filterByType,
    sortBy,
    goToPage,
    resetFilters
  };
};

export const fetchAndLoadResults = (url, opts, dispatch) => {
  fetch(url, opts)
    .then(res => res.json())
    .then(data => {
      dispatch({
        type: "LOAD_RESULTS",
        payload: data
      });
    })
    .catch(console.error);
};
