import { initialState } from "../constants";

const FlaconiUiReducer = (state, action) => {
  switch (state.uistatus) {
    case "loaded": {
      switch (action.type) {
        case "TRIGGER_LOAD": {
          return { ...state, uistatus: "triggerload" };
        }
        case "FILTER_BY_BRAND": {
          return {
            ...state,
            filters: {
              ...state.filters,
              brand: action.payload.brand
            },
            uistatus: "triggerload"
          };
        }
        case "FILTER_BY_TYPE": {
          return {
            ...state,
            filters: {
              ...state.filters,
              type: action.payload.type
            },
            uistatus: "triggerload"
          };
        }
        case "SORT_BY": {
          return {
            ...state,
            filters: {
              ...state.filters,
              sort: action.payload.sort
            },
            uistatus: "triggerload"
          };
        }
        case "NAVIGATE_TO_PAGE": {
          return {
            ...state,
            filters: {
              ...state.filters,
              page: action.payload.page
            },
            uistatus: "triggerload"
          };
        }
        case "RESET_FILTERS": {
          return {
            ...state,
            filters: initialState.filters,
            uistatus: "triggerload"
          };
        }
        default: {
          return state;
        }
      }
    }
    default: {
      switch (action.type) {
        case "LOADING": {
          return { ...state, uistatus: "loading" };
        }
        case "LOAD_RESULTS": {
          return { ...state, ...action.payload, uistatus: "loaded" };
        }

        case "LOAD_ALL_TYPES_AND_BRANDS": {
          return {
            ...state,
            ...action.payload
          };
        }

        default: {
          return state;
        }
      }
    }
  }
};

export default FlaconiUiReducer;
