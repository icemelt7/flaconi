export const url = process.env.REACT_APP_CI
  ? "https://server-ufdmcfqtyz.now.sh"
  : "http://localhost:4000";
export const ITEMS_PER_PAGE = 4;
export const initialState = {
  filters: {
    page: 1,
    sort: "bestRated"
  },
  uistatus: "loaded"
};