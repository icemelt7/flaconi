const theme = {
  text: "#484c7f",
  bg: "#f1d4d4",
  light: "#ddb6c6",
  dark: "#ac8daf"
}

export default theme;