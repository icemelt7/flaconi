import React from 'react';
import {render, waitForElement, fireEvent } from '@testing-library/react'

import App from './App';

import '@testing-library/jest-dom/extend-expect'

it('renders without crashing, and API is working', async () => {
  const { getByText, getAllByTestId } = render(
    <App />
  )

  await waitForElement(() =>
  // getByTestId throws an error if it cannot find an element
    getByText('All Items')
  )

  expect(getAllByTestId('list-item').length).toBe(4);

  expect(getAllByTestId('brand').length).toBeGreaterThan(0);
  
  expect(getAllByTestId('type').length).toBeGreaterThan(0);

});

it('Brand and type filters work as expected', async () => {
  const { findByText, getByText, getAllByText, getByTestId } = render(
    <App />
  )

  await waitForElement(() =>
  // getByTestId throws an error if it cannot find an element
    getByText('All Items')
  )
  

  fireEvent.click(getAllByText('Issey Miyake')[0]);

  await waitForElement(() =>
  // getByTestId throws an error if it cannot find an element
    getByText('All Issey Miyake items')
  )

  expect(getByTestId('seo')).toHaveTextContent('All Issey Miyake items')

  fireEvent.click(getAllByText('Eau de Parfum')[0]);
  
  await waitForElement(() =>
  // getByTestId throws an error if it cannot find an element
    getByText('Eau de Parfums By Issey Miyake')
  )

  expect(getByTestId('seo')).toHaveTextContent('Eau de Parfums By Issey Miyake')
})