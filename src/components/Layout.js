import React from 'react';
import styled from 'styled-components';

const StyledLayout = styled.div`
  @media(min-width: 375px) {
    max-width: 350px;
  }
  @media(min-width: 768px) {
    max-width: 640px;
  }
  @media(min-width: 1024px) {
    max-width: 850px;
  }
  margin: 40px auto;
`
const Layout = ({ children }) => {
  return <StyledLayout>{children}</StyledLayout>
}

export default Layout;