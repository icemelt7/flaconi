import React from 'react';
import styled from 'styled-components';
import theme from '../../theme';

const StyledSearchBar = styled.input`
  border: none;
  background-color: white;
  color: ${theme.text};
  margin: 5px 5px;
  height: 25px;
`

const SearchButton = styled.button`
  border: none;
  background-color: ${theme.dark};
  color: ${theme.bg};
  margin: 5px 5px;
  height: 26px;
`
const StyledSearchBarHolder = styled.div`
  margin-left: auto;
`
const SearchBar = () => {
  return <StyledSearchBarHolder>
    <StyledSearchBar />
    <SearchButton>Search</SearchButton>
  </StyledSearchBarHolder>
}

export default SearchBar;