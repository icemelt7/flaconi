import React from 'react';
import styled from 'styled-components';

const StyledSlogan = styled.div`
  padding: 10px;
  font-weight: 600;
  font-size: 16px;
`
const Slogan = () => {
  return <StyledSlogan>Home of Parfum - Best Price</StyledSlogan>
}

export default Slogan;