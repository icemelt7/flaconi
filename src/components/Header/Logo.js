import React from 'react';
import styled from 'styled-components';
import theme from '../../theme';

const StyledLogo = styled.div`
  color: ${theme.bg};
  background-color: ${theme.text};
  padding: 10px;
  margin-right: 20px;
  font-weight: 600;
  font-size: 16px;
`
const Logo = () => {
  return <StyledLogo>Flaconi</StyledLogo>
}

export default Logo;