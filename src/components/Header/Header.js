import React from 'react';
import styled from 'styled-components';
import theme from '../../theme';

const StyledHeader = styled.div`
  display: flex;
  padding-bottom: 12px;
  margin-bottom: 12px;
  border-bottom: 4px solid ${theme.text}
` 
const Header = ({ children }) => {
  return <StyledHeader>{children}</StyledHeader>
}

export default Header;