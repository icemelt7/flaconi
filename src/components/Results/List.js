import React from "react";
import styled from "styled-components";
import theme from "../../theme";
import NoResults from "./NoResults";
import Loading from "./Loading";

const Card = styled.li`
  max-width: 248px;
  display: flex;
  flex-wrap: wrap;
  align-items: baseline;
  justify-content: center;
  padding: 24px;
  margin-bottom: 36px;
  box-shadow: 0 0 32px 0 #ffbcbc21;
  border-radius: 10px;
  background-color: white;
  float: left;
  @media (min-width: 768px) {
    margin-right: 24px;
  }
`;
const SeoHeading = styled.h1`
  font-size: 24px;
  text-align: center;
`;
const UnStyledList = styled.ul`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  list-style: none;
  margin: 0;
  padding: 0;
`;
const ItemTitle = styled.a`
  padding: 12px 24px;
  text-align: center;
  font-size: 18px;
  text-decoration: none;
  font-weight: 400;
  color: inherit;

  :hover {
    text-decoration: underline;
  }
`;

const Price = styled.span`
  margin-bottom: 12px;
  width: 100%;
  text-align: center;
  font-size: 18px;
  font-weight: 600;
`;
const By = styled.span`
  font-size: 14px;
  margin: 0 6px;
`;
const Ml = styled.span`
  color: ${theme.dark};
  font-size: 14px;
  text-align: center;
  width: 100%;
`;
const Brand = styled.span`
  color: ${theme.dark};
  margin-bottom: 12px;
`;
const Type = styled.span`
  color: ${theme.dark};
`;
const Rating = styled.span`
  color: ${theme.dark};
  font-size: 14px;
  text-align: center;
  width: 100%;
`;
const List = ({ uistatus, resetFilters, filters, data }) => {
  let seoHeading = "All Items";
  if (filters.type && !filters.brand) {
    seoHeading = `All ${filters.type}s`;
  }
  if (!filters.type && filters.brand) {
    seoHeading = `All ${filters.brand} items`;
  }
  if (filters.type && filters.brand) {
    seoHeading = `${filters.type}s By ${filters.brand}`;
  }
  return (
    <div>
      {uistatus === "loading" ? (
        <Loading />
      ) : (
        <>
          <SeoHeading data-testid="seo">{seoHeading}</SeoHeading>
          {data && data.products && data.products.length ? (
            <UnStyledList>
              {data.products.map((item, i) => (
                <Card data-testid="list-item" key={item.slug}>
                  <img src={item.image} alt={item.name} />
                  <ItemTitle href={item.slug}>{item.name}</ItemTitle>
                  <Type>{item.type}</Type>
                  <By>by</By>
                  <Brand>{item.brand}</Brand>
                  <Price>
                    {new Intl.NumberFormat("de-DE", {
                      style: "currency",
                      currency: "EUR"
                    }).format(item.price)}
                  </Price>
                  <Ml>Bottle Size: {item.size}</Ml>
                  <Rating>Rating: {item.rating}/100</Rating>
                </Card>
              ))}
            </UnStyledList>
          ) : (
            <NoResults resetFilters={resetFilters} />
          )}
        </>
      )}
    </div>
  );
};

export default List;
