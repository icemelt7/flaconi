import React from 'react';
import styled from 'styled-components';
import theme from '../../theme';

const Heading = styled.h3`
  font-size: 16px;
  margin: 0 0 4px;
  padding: 0;
`
const PillBox = styled.ul`
  display: flex;
  flex-wrap: wrap;
  margin: 0 0 12px;
  padding: 0;
  list-style: none;
`

const Item = styled.li`
  text-decoration: none;
`
const ItemLink = styled.a`
  display: block;
  font-size: 12px;
  background-color: ${props => props.selected ? theme.bg : `white`};
  border: 1px solid ${theme.dark};
  border-radius: 20px;
  padding: 4px 8px;
  margin: 0 4px 8px 0;
  text-decoration: none;

  :hover {
    background-color: ${theme.bg};
  }
`
const TypeSelector = ({ types, filterByType, selectedType }) => {
  return <>
  <Heading>
    Item Type
  </Heading>
  <PillBox>
    {types && types.map(type => <Item key={type}>
      <ItemLink data-testid="type" selected={type === selectedType} onClick={e => filterByType(type)}>
        {type}
      </ItemLink>
    </Item>)}
  </PillBox>
</>
}

export default TypeSelector;