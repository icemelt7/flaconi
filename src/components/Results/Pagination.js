import React from 'react';
import styled from 'styled-components';
import theme from '../../theme';

const PageButton = styled.button`
  background-color: ${theme.bg};
  border: none;
  padding: 8px 16px;
  border-radius: 6px;
  margin: 0 20px;
  font-size: 16px;
  color: ${theme.text};  
  :hover {
    background-color: ${theme.light};
  }
`;

const FlexCenter = styled.div`
  display: flex;
  justify-content: center;
  align-items: baseline;
`

const Pagination = ({length, goToPage, page}) => {
  if (page === 1){
    return <FlexCenter>
      <PageButton onClick={e => goToPage(page+1)}>Next Page</PageButton>
    </FlexCenter>
  }
  if (length === 0) {
    return <FlexCenter>
      <PageButton onClick={e => goToPage(page-1)}>Previous Page</PageButton>
    </FlexCenter>
  }
  return <FlexCenter>
  <PageButton onClick={e => goToPage(page-1)}>Previous Page</PageButton>
  {page}
  <PageButton onClick={e => goToPage(page+1)}>Next Page</PageButton>
  </FlexCenter>
  
}

export default Pagination;