import React from 'react';
import styled from 'styled-components';

const StyleLoading = styled.div`
  text-align: center;
  font-size: 24px;
  margin: 20px 0;
`
const Loading = () => {
  return <StyleLoading>Loading</StyleLoading>
}

export default Loading;