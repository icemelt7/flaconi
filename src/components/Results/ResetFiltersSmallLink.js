import React from "react";
import styled from "styled-components";

const ResetButton = styled.a`
  display: block;
  margin: 0 0 24px;
  text-decoration: underline;
  font-size: 12px;
  :hover {
    cursor: pointer;
  }
`;

const ResetFiltersSmallLink = ({ resetFilters }) => {
  return (
    <ResetButton onClick={resetFilters}>Reset Filters</ResetButton>
  );
};

export default ResetFiltersSmallLink;
