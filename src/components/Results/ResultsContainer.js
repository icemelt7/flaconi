import React from 'react';

const ResultsContainer = ( {children }) => {
  return <div>{children}</div>
}

export default ResultsContainer;