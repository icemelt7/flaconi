import React from "react";
import styled from "styled-components";
import theme from "../../theme";

const SpacedOutText = styled.div`
  margin: 40px auto;
  font-size: 24px;
`;

const ResetButton = styled.button`
  background-color: ${theme.dark};
  color: white;
  border: none;
  border-radius: 8px;
  font-size: 24px;
  padding: 10px 20px;
  margin-bottom 40px;
`;

const NoResults = ({ resetFilters }) => {
  return (
    <div style={{textAlign: 'center'}}>
      <SpacedOutText>
        No Reults were found :(
        <br />
        Try to{" "}
      </SpacedOutText>
      <ResetButton onClick={resetFilters}>Reset Filters</ResetButton>
    </div>
  );
};

export default NoResults;
