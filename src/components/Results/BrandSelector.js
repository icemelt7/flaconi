import React from "react";
import styled from "styled-components";
import theme from "../../theme";
const Heading = styled.h3`
  font-size: 16px;
  margin: 0 0 4px;
  padding: 0;
`;

const List = styled.ul`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  @media (min-width: 768px) {
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
  }
  grid-column-gap: 6px;
  grid-row-gap: 8px;
  margin: 0 0 12px;
  padding: 0;
  list-style: none;
`;

const Brand = styled.a`
  background-color: ${props => (props.selected ? theme.light : theme.dark)};
  color: white;
  padding: 4px 6px;
  border-radius: 4px;
  height: 100%;
  box-sizing: border-box;
  display: block;
  font-size: 14px;
  text-decoration: none;
  :hover {
    background-color: ${theme.light};
  }
`;

const BrandSelector = ({ brands, filterByBrand, selectedBrand }) => {
  return (
    <>
      <Heading>Brands</Heading>
      <List>
        {brands &&
          brands.map(brand => (
            <li key={brand}>
              <Brand
                data-testid="brand"
                selected={brand === selectedBrand}
                onClick={e => filterByBrand(brand)}
              >
                {brand}
              </Brand>
            </li>
          ))}
      </List>
    </>
  );
};

export default BrandSelector;
