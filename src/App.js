import React from "react";
import useFlaconiBackend from "./hooks/FlaconiBackend";
import Layout from "./components/Layout";

import Header from "./components/Header/Header";
import Logo from "./components/Header/Logo";
import SearchBar from "./components/Header/SearchBar";

import ResultsContainer from "./components/Results/ResultsContainer";
import BrandSelector from "./components/Results/BrandSelector";
import TypeSelector from "./components/Results/TypeSelector";
import Pagination from "./components/Results/Pagination";

import List from "./components/Results/List";
import SortSelector from "./components/Results/SortSelector";
import ResetFiltersSmallLink from "./components/Results/ResetFiltersSmallLink";
import Loading from "./components/Results/Loading";

const App = () => {
  const {
    values,
    filterByBrand,
    filterByType,
    goToPage,
    sortBy,
    resetFilters
  } = useFlaconiBackend();
  
  return (
    <Layout>
      <Header>
        <Logo />
        <SearchBar />
      </Header>
      {!values.data && values.uistatus === "loading" && <Loading />}
      {values.data && (
        <ResultsContainer>
          <TypeSelector
            types={values.types}
            filterByType={filterByType}
            selectedType={values.filters.type}
          />
          <BrandSelector
            brands={values.brands}
            filterByBrand={filterByBrand}
            selectedBrand={values.filters.brand}
          />
          <SortSelector sortBy={sortBy} sort={values.filters.sort} />
          <ResetFiltersSmallLink resetFilters={resetFilters} />
          <List
            uistatus={values.uistatus}
            resetFilters={resetFilters}
            filters={values.filters}
            data={values.data}
          />
          <Pagination
            length={values.data.products.length}
            goToPage={goToPage}
            page={values.filters.page}
          />
        </ResultsContainer>
      )}
    </Layout>
  );
};

export default App;
