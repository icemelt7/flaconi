#!/bin/bash

cp -r ./build ./flaconi-build
mv ./flaconi-build ./build
mv ./build/flaconi-build ./build/flaconi
npx serve build -p 3000 &
node server/index.js