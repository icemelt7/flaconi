const { ApolloServer, gql } = require('apollo-server');
const data = require('./productlist');

// Type definitions define the "shape" of your data and specify
// which ways the data can be fetched from the GraphQL server.
const typeDefs = gql`
  # Comments in GraphQL are defined with the hash (#) symbol.

  # This "Product" type can be used in other type declarations.
  type Product {
    id: String
    name: String
    slug: String
    brand: String
    type: String
    image: String
    price: Int
    size: String
    rating: Int
  }

  # The "Query" type is the root of all GraphQL queries.
  # (A "Mutation" type will be covered later on.)
  type Query {
    products(offset: Int, limit: Int, brand: String, type: String, sort: String): [Product]
    brands: [String]
    types: [String]
  }
`;

// Resolvers define the technique for fetching the types in the
// schema.  We'll retrieve books from the "books" array above.
const resolvers = {
  Query: {
    products: (parent, { limit = 4, offset = 0, brand, type, sort = "bestRated" }) => {
      let result = data;
      if (brand) {
        result = result.filter(product => product.brand === brand);
      }

      if (type) {
        result = result.filter(product => product.type === type);
      }

      if (sort) {
        result.sort((a, b) => {
          if (sort === "bestRated") {
            if (a.rating > b.rating) return -1;
            if (a.rating < b.rating) return 1;
            return 0;
          }

          if (sort === "priceLowToHigh") {
            if (a.price > b.price) return 1;
            if (a.price < b.price) return -1;
            return 0;
          }

          if (sort === "priceHighToLow") {
            if (a.price > b.price) return -1;
            if (a.price < b.price) return 1;
            return 0;
          }
          return 0;
        });
      }
      return result.slice(offset, offset + limit);
    },

    brands: () => {
      return [...new Set(data.map( product => product.brand ))]
    },

    types: () => {
      return [...new Set(data.map( product => product.type ))]
    }
  },
};

// In the most basic sense, the ApolloServer can be started
// by passing type definitions (typeDefs) and the resolvers
// responsible for fetching the data for those types.
const server = new ApolloServer({ typeDefs, resolvers });

// This `listen` method launches a web-server.  Existing apps
// can utilize middleware options, which we'll discuss later.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});